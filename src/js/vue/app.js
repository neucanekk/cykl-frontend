var uploadIcon = Vue.component('upload', {
    template: `<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="feather feather-upload">
        <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
        <polyline points="17 8 12 3 7 8"></polyline>
        <line x1="12" y1="3" x2="12" y2="15"></line>
    </svg>
    `
});

var uploadItem = Vue.component('uploaditem', {
    props: ['id', 'name'],
    template: `<label>
        <input :id="id" type="file" class="user-image-input" :name="name">
        <div class="custom-upload">
            <upload />
        </div>
    </label>`
});

var galleryItem = Vue.component('galleryitem', {
    props: ['itemid', 'selectitem'],
    template: `<div :id="itemid" class="app-gallery-item" @click="selectitem"><slot></slot></div>`
});

var galleryItemThumb = Vue.component('thumb', {
    props: ['source', 'title'],
    template: `<img :src="source" :alt="title" class="gallery-item-thumb">`,
});

var wordCount = Vue.component('wordcount', {
    props: ['charlen', 'limit'],
    template: `<p v-if="charlen > 0">{{ charlen }} / {{ limit }} znaków</p>
    <p v-else>max. {{ limit }} znaków</p>`
})

var app = new Vue({
    // delimiters: ['{$', '$}'],
    components: {
        uploadIcon,
        uploadItem,
        galleryItem,
        galleryItemThumb,
        wordCount
    },
    data() {
        return {
            isMobile: false,
            stageLevel: 0,
            selection: [],
            uploadLimit: 5,
            limit: 15,
            wishes: `Zdrowych, 
            radosnych Świąt Bożego 
            Narodzenia.

            Miłości i wzruszeń,
            radości i spełnienia 
            marzeń oraz gwiazdki,
            która doprowadzi wszystkich
            do domu.`,
            wishesTextLimit: 150
        }
    },
    mounted() {
        window.addEventListener('resize', () => {
            this.mediaQuery();
            this.showStage(this.stageLevel);
        });

        this.mediaQuery();
        this.showStage(this.stageLevel);        
    },
    computed: {},
    methods: {
        selected(e) {
            const el = e.path[0];
            const id = el.id;        
            const galleryItems = document.querySelectorAll('.app-gallery-item');  

            const elementById = (element) => { return document.getElementById(element) };

            const previewModal = elementById('preview-image');
            const sourceImage = elementById('source-image');
            const previewButtonSelect = elementById('select-gallery-image');

            let arr = [];
            // const limitReachedContext = elementById('limited-selection');
        
            galleryItems.forEach(item => { item.classList.remove('preview'); });
            el.classList.add('preview');
                        
            if(el.classList.contains('preview')) {
                previewModal.classList.add('active-preview');
                sourceImage.attributes.src.nodeValue = el.children[0].attributes.src.nodeValue;

                previewModal.setAttribute('data-preview', id);
                previewButtonSelect.setAttribute('data-select', id);                                
            }            

            if(el.classList.contains('selected') && el.classList.contains('preview')) {
                previewButtonSelect.classList.add('preview-button-selected');
                previewButtonSelect.lastElementChild.innerHTML = 'Wybrano';
            } else {
                previewButtonSelect.classList.remove('preview-button-selected');
                previewButtonSelect.lastElementChild.innerHTML = 'Wybierz zdjęcie';
            }

            const clearModal = () => {
                previewModal.removeAttribute('data-preview');
                previewModal.classList.remove('active-preview');
                previewButtonSelect.removeAttribute('data-select');

                console.log('current', arr);
            }            

            // previewButtonSelect.addEventListener('click', () => {                                  
            //     if(el.classList.contains('selected')) {                    
            //         previewButtonSelect.classList.remove('preview-button-selected');
            //         previewButtonSelect.lastElementChild.innerHTML = 'Wybierz zdjęcie';

            //         this.selection = this.selection.filter(item => item !== id);
            //     } else {
            //         el.classList.add('selected');
            //         previewButtonSelect.classList.add('preview-button-selected');
            //         previewButtonSelect.lastElementChild.innerHTML = 'Wybrano';

            //         this.selection.push(id);
            //     }
                                
            //     el.classList.replace('preview', 'selected');

            //     clearModal();
            // });

        },
        closeModal() {
            const previewModal = document.getElementById('preview-image');
            const previewButtonSelect = document.getElementById('select-gallery-image');
            const thumbImage = document.querySelector('.app-gallery-item');

            previewModal.classList.remove('active-preview');
            thumbImage.classList.remove('preview');

            previewModal.removeAttribute('data-preview');
            previewButtonSelect.removeAttribute('data-select');
        },
        mediaQuery() {
            var windowWidth = document.documentElement.clientWidth;

            if(windowWidth > 992) {
                this.isMobile = false;
            } else {
                this.isMobile = true;
            }
        },

        showStage(stage) {
            if(stage === 1) {
                this.stageLevel = 1;
            } else if(stage === 2) {
                this.stageLevel = 2;
            } else if(stage === 3) {
                this.stageLevel = 3;
            } else {
                this.stageLevel = 0;
            }
        }
    },
});

app.$mount('#app');