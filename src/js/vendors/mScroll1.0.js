
var mobilePoint = 768, //less then 768
    scrollTime = 2000,
    windowWidth = $(window).width();

$(window).resize(function() {
    windowWidth = $(window).width();
});

// set offset

function setOffset(offset, mobileOffset) {
    if(offset === undefined) {
        offset = 0;
    }

    if(mobileOffset === undefined) {
        mobileOffset = 0;
    }

    if(windowWidth < mobilePoint) {
        return mobileOffset
    } else {
        return offset
    }
}

// add active class to menu links

var sections = $('.nav-section'),
    nav = $('.menu-links'),
    nav_height = nav.outerHeight();

$(window).on('scroll', function () {
    var cur_pos = $(this).scrollTop();

    sections.each(function() {
        var offset = $(this).attr('data-offset');
        var mobileOffset = $(this).attr('data-mobile-offset');
        var offsetComputed = setOffset(offset, mobileOffset);
        var top = $(this).offset().top - nav_height - offsetComputed,
            bottom = top + $(this).outerHeight();

        if (cur_pos >= top && cur_pos <= bottom) {
            nav.find('a').removeClass('active');
            sections.removeClass('active');

            $(this).addClass('active');
            nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
        }
    });
});

// scroll to sections on menu link clicks

nav.find('a').on('click', function () {
    var $el = $(this),
        id = $el.attr('href'),
        offset = $(id).attr('data-offset'),
        mobileOffset = $(id).attr('data-mobile-offset');

    var offsetComputed = setOffset(offset, mobileOffset);

    $('html, body').animate({
        scrollTop: $(id).offset().top - nav_height - offsetComputed
    },scrollTime);

    //return false;
});

// paging

function getActivePage() {
    return nav.find('a.active').closest('.item');
}

$('.page-links .page-up').on('click', function(e){
    var link = getActivePage().prev().find('a');
    $(link).trigger('click');
    console.log(link);

   e.preventDefault();
});

$('.page-links .page-down').on('click', function(e){
    var link = getActivePage().next().find('a');
    $(link).trigger('click');
    console.log(link);
    e.preventDefault();
});



