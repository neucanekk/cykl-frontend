'use strict';

var textOrphans = function () {
    $('p').each(function () {

        if ($(this).html().match(/class="order"/g)) return;

        var tekst = $(this).html();
        tekst = tekst.replace(/(\s)([\S])[\s]+/g, "$1$2&nbsp;"); //jednoznakowe
        tekst = tekst.replace(/(\s)([^<][\S]{1})[\s]+/g, "$1$2&nbsp;"); //dwuznakowe
        $(this).html(tekst);
    });
};

var cookies = function () {
    if ($.cookie('giodoCookies') !== '1') {
        $('.cookies-alert').css('display', 'block');
    }
    $('.cookies-alert button').on('click', function () {
        $('.cookies-alert').slideToggle('slow', function () {
            $.cookie('giodoCookies', '1', {
                expires: 365
            });
        });
    });
};

var santaAnimationPlaceholder = function () {
    const player = document.querySelector("lottie-player");
    player.load('../json/santa-placeholder.json');
}

var countdown = function () {
    var countDownDate = new Date('Dec 1, 2021 00:00:00').getTime();

    var x = setInterval(function () {
        var now = new Date().getTime();

        var distance = countDownDate - now;

        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        var leadingZero = function (count, size) {
            count = count.toString();
            while (count.length < size) count = "0" + count;
            return count;
        }
        var countDays = $('#days');
        var countHours = $('#hours');
        var countMinutes = $('#minutes');
        var countSeconds = $('#seconds');

        countDays.text(leadingZero(days, 2));
        countHours.text(leadingZero(hours, 2));
        countMinutes.text(leadingZero(minutes, 2));
        countSeconds.text(leadingZero(seconds, 2));

        if (distance < 0) {
            clearInterval(x);
        }
    }, 1000);
}

var controller;
var animationOnScroll = function () {
    controller = new ScrollMagic.Controller();

    var introScene = function () {
        // Vars
        let countItems = [];
        let images = [];

        for (var i = 1; i < 85; i++) {
            countItems.push(i);
        }

        function zeroPad(num) {
            return num.toString().padStart(3, "0");
        }

        var numbers = countItems;

        numbers.forEach(
            function (num) {
                var paddedNum = zeroPad(num);
                var path = "/img/sequence/santa-cropped/santa_animation_" + paddedNum + '.png';

                images.push(path);
            }
        );

        var obj = {
            curImg: 0
        };

        var tweens = new TimelineMax()
            .add(TweenMax.to(obj, 5, {
                curImg: images.length - 1,
                roundProps: 'curImg',
                repeat: 0,
                immediateRender: true,
                ease: Linear.easeNone,
                onUpdate: function () {
                    $("#santaSequence").attr("src", images[obj.curImg]);
                },
                delay: 0
            }));

        var scene = new ScrollMagic.Scene({
                triggerHook: 'onLeave',
                triggerElement: '.section-first',
                duration: '100%'
            })
            .setTween(tweens)
            .setPin('.section-first', {
                pushFollowers: true
            })
            .addTo(controller);
    }

    var WishcardScene = function () {
        var header = $('#sectionTwoHeader');
        var instruction = $('#sectionTwoInstruction .item');
        var wishcard = $('#sectionTwoWishcard');

        var sceneHeadingSectionTwo = new ScrollMagic.Scene({
                triggerElement: '.section-second',
                duration: $('.container-heading').innerHeight()
            })
            .setTween(TweenMax.from(header, .2, {
                autoAlpha: 0,
                y: '+=25',
                ease: Linear.easeNone
            }, 1))
            .addTo(controller);

        instruction.each(function (i, el) {
            var sceneInstruction = new ScrollMagic.Scene({
                triggerHook: .8,
                triggerElement: el,
                duration: $(this).innerHeight()
            }).setTween(TweenMax.from(this, .2, {
                autoAlpha: 0,
                x: '+=25',
                ease: Linear.easeNone
            }, 1)).addTo(controller)
        })

        var sceneWishcard = new ScrollMagic.Scene({
            triggerHook: 'onEnter',
            triggerElement: '#sectionTwoWishcard',
        }).setTween(TweenMax.from(wishcard, .2, {
            autoAlpha: 0,
            ease: Linear.easeNone
        }, 1)).addTo(controller);
    }

    introScene();
    WishcardScene();
}

var christmasBalls = function() {
    var svg = $('#xmas-ball-map');
    var svgCircle = svg.find('circle');

    svgCircle.each((index, circle) => {
        var getId = circle.id;
        var n = getId.substr(6);
        var targetBall = $('.xmas-ball#xmasball_'+n);

        var posX = target => target.cx.baseVal.valueAsString / 1;
        var posY = target => target.cy.baseVal.valueAsString / 1;
        var size = target => target.getBoundingClientRect().width;
        var roundNumber = target => Math.floor(Math.round(target));

        targetBall.css({
            top: roundNumber(posY(circle)),
            left: roundNumber(posX(circle)),
            maxWidth: roundNumber(size(circle)),
            height: roundNumber(size(circle)),
        });
    })
}

var wishes = function () {
    var area = $('#enter-wishes');
    var wordCount = $('#wordcount');
    var defaultWishes = $('#wishes-results p');
    var countCurrent = 0;
    var countLimit = 200;

    var reset = function () {
        area.removeClass('show');
        $('#xmas-wishes').val('');
        countCurrent = 0;
        wordCount.text(0);
    }

    $('#wordlimit').text(countLimit);
    $('#xmas-wishes').attr('maxlength', countLimit);

    $('#wishes-results').on('click', function () {
        $('#enter-wishes').addClass('show');
        var current = defaultWishes[0].outerText;
        $('#xmas-wishes').val(current);
        wordCount.text(current.length);
    })

    $('#xmas-wishes').on('keyup', function () {
        countCurrent = $(this).val().length;
        wordCount.text(countCurrent);
    });

    $('#wishes-reset').on('click', function () {
        reset();
    });

    $('#wishes-confirm').on('click', function () {
        var wishes = $('#xmas-wishes').val();
        var wishesFormatted = wishes.replace(/\n/g, '<br />');

        $('#wishes-results p').html(wishesFormatted);

        reset();
    });
}

var destroyScrollMagic = function () {
    if (controller) {
        controller = controller.destroy(true);

        // Remove any pin-spacers
        $('.scrollmagic-pin-spacer').contents().unwrap();

        //clear all extra inline styles
        $('.page-section__bg').each(function () {
            var $this = $(this);
            var background = $this.css('background-image');

            $this.removeAttr('style').css('background-image', background);
        });
    }
} // END destroyScrollMagic

let selectedLimit = 3;
let selectedGalleryItems = [];
var previewThumbImage = function() {
    var thumbs = $('.gallery-item');

    thumbs.each(function() {
        $(this).on('click', function() {
            // Add preview class to chosen thumb image
            $(this).addClass('preview');

            var getImgSrc = $(this).find('img').attr('src');
            var preview = $('#preview-image');
            var previewModal = $('#large-image');
            var previewId = $(this).prop('id').substr(7);
            var targetButton = $('#select-gallery-image');
            var targetButtonText = targetButton.find('span');
            var targetButtonIcon = targetButton.find($('.icon'));
            var limitReachedContent = $('#limited-selection');

            var buttonSettings = function(callback) {
                if(callback === true) {
                    targetButton.addClass('preview-button-selected');
                    targetButtonIcon.show();
                    targetButtonIcon.css('display', 'flex');
                    targetButtonText.text('Zdjęcie wybrane');
                } else {
                    targetButton.removeClass('preview-button-selected');
                    targetButtonIcon.hide();
                    targetButtonText.text('Wybierz zdjęcie');
                }
            }

            // Change preview img src
            preview.attr({ src: getImgSrc });

            // Show preview and add active preview class
            previewModal.fadeIn(300);
            previewModal.attr('data-preview', previewId).addClass('active-preview');

            // Add preview image id to the button
            targetButton.attr('data-select', previewId);

            // Reduce opacity of all thumb items except currently previewed one
            if(previewModal.hasClass('active-preview')) {
                $('.gallery-item').not(this).css({opacity: 0.5});
            }

            // Check if preview thumb is selected 
            if($(this).hasClass('selected')) {
                buttonSettings(true);
            } else {
                buttonSettings(false);
            }

            // Check if selection limit has been reached
            if(selectedGalleryItems.length === selectedLimit) {
                targetButton.hide();
                limitReachedContent.show();

                if($(this).hasClass('selected')) {
                    targetButton.show();
                    limitReachedContent.hide();
                }
            } else {
                targetButton.show();
                limitReachedContent.hide();
            }


        });
    });
}

$('#play-video').on('click', function() {
    var videoContainer = $('#video-container');
    var video = $('#video');
    var volumeNav = $('#volume-settings');
    var volumeIconOn = volumeNav.find($('.volume-on'));
    var volumeIconOff = volumeNav.find($('.volume-off'));

    $('.control-play').removeClass('visible');
    videoContainer.removeClass('background-video');
    video.removeAttr('muted');
    video.prop({muted: false, autoplay: false, loop: false, controls: true});


    if(video.prop('muted')) {
        volumeIconOn.addClass('hidden');
        volumeIconOff.removeClass('hidden');
    } else {
        volumeIconOff.addClass('hidden');
        volumeIconOn.removeClass('hidden');
    }
});

$('#large-image .close').on('click', function() {
    $('.gallery-item').removeClass('preview');
    $('#large-image').removeClass('active-preview');
    $('#large-image').fadeOut(300);

    $('.gallery-item').css({opacity: 1});
});

$('#close-crop-modal').on('click', function(e) {
    if($('.crop-image-modal').hasClass('active')) {
        $('.crop-image-modal').removeClass('active').removeAttr('data-id');
        $('.crop-result img').remove();
    }
});

let cropper = null;
$('#crop-preview-return').on('click', function(e) {
    // Content
    if($('.crop-image-preview').hasClass('active')) {
        $('.crop-image-preview').removeClass('active');
        cropper.destroy();
    }
});

$('.cropped-item-delete').on('click', function(e) {
    var that = $(this);
    var parent = that.closest($('.cropped-item-image'));
    var parentWrapper = that.closest($('.upload-list-item'));
    if(parentWrapper && parent) {
        var inputUpload = parentWrapper[0].querySelector('label');
        var imageWrapper = parentWrapper[0].querySelector('.cropped-item-wrapper');
        var imageItem = imageWrapper.querySelector('img');
        that.siblings(imageItem.remove());
        parent[0].classList.remove('show');
        inputUpload.style.display = '';
    }
})

$('#crop-image-start').on('click', function(e) {
    e.preventDefault();

    var modal = $('.crop-image-modal.active');
    var modalDataId = modal.prop('data-id');
    var modalImageView = modal.find('img')[0];

    var preview = $('.crop-image-preview');
    var previewImg = preview.find('img');

    preview.addClass('active').attr('data-previewid', modalDataId);
    previewImg.draggable = false;

    previewImg.attr('src', modalImageView.currentSrc);

    cropper = new Cropper(previewImg[0], {
        aspectRatio: 1 / 1,
        minCropBoxWidth: 50,
        maxCropBoxWidth: 300,
        maxContainerHeight: 540,
        rotatable: false
    });
});


$('#crop-image-select').on('click', function() {
    var cropped = $('.image-cropped');
    var parent = $(this).closest($('.crop-image-modal'));
    var targetElement = parent[0].dataset;
    var currentUploadInput = $('#'+targetElement.id).closest($('.upload-list-item'));
    let croppedSrc = cropper.getCroppedCanvas({ width: cropped.value }).toDataURL('image/jpeg');
    var itemInput = currentUploadInput.find('label');
    if(currentUploadInput) {
        var croppedItemWrapper = currentUploadInput[0].querySelector('.cropped-item-image');
        var croppedImageWrapper = currentUploadInput[0].querySelector('.cropped-item-wrapper');
        if (croppedItemWrapper && croppedImageWrapper) {
            croppedItemWrapper.classList.add('show');
            var oldItemId = targetElement.id;
            var splitItem = oldItemId.split('img-')
            var img = document.createElement('img');
            img.classList.add('user-gallery-item');
            img.classList.add('gallery-item-thumb');
            img.src = croppedSrc;
            img.alt = 'thumb ' + splitItem[1];
            img.setAttribute('data-id', 'user-i' + splitItem[1]);
            croppedImageWrapper.appendChild(img);
            itemInput.hide();
            cropper.destroy();
            $('.crop-image-modal').removeClass('active');
            $('.crop-image-preview').removeClass('active');
            $('.crop-result > img').remove();
            $('.crop-image-view > img').attr('src', '');
            $('.crop-image-close').removeAttr('close');
        }
    }
});


$('#select-gallery-image').on('click', function() {
    var buttonText = $(this).find('span');
    var icon = $(this).find($('.icon'));
    var buttonTargetId = $(this)[0].dataset.select;
    var galleryThumb = $('#thumbg_'+buttonTargetId);

    $(this).toggleClass('preview-button-selected');

    if($(this).hasClass('preview-button-selected')) {
        icon.show();
        icon.css('display', 'flex');
        buttonText.text('Zdjęcie wybrane');

        galleryThumb.addClass('selected');
    } else {
        icon.hide();
        buttonText.text('Wybierz zdjęcie');

        galleryThumb.removeClass('selected');
    }

    if(galleryThumb.hasClass('selected')) {
        selectedGalleryItems.push(buttonTargetId)
    } else {
        selectedGalleryItems = selectedGalleryItems.filter(item => item !== buttonTargetId);
    }
});


$('#volume-settings').on('click', function () {
    if ($('#video').prop('muted')) {
        $('#video').prop('muted', false);
        $('.volume-off').addClass('hidden');
        $('.volume-on').removeClass('hidden');
    } else {
        $('#video').prop('muted', true);
        $('.volume-on').addClass('hidden');
        $('.volume-off').removeClass('hidden');
    }
});

$('.menu-nav:not(.menu-nav-ar) ul li a').on('click', function(e){
    e.preventDefault();
    var that = $(this).attr('href');

    $('html, body').animate({
        scrollTop: $(that).offset().top
    }, 'slow');
});

$('#scroll-trigger a').on('click', function(e) {
    e.preventDefault();
    var target = $(this).attr('href');

    $('html, body').animate({
        scrollTop: $(target).offset().top
    }, 'slow');
});

$('#card_download').on('click', e => e.preventDefault());

$(window).on('resize', function() {
    if($(window).width() > 992) {
        if($('.main-nav').hasClass('is-scrolling')) {
            $('.main-nav').removeClass('is-scrolling');
        }
        
        christmasBalls();
    }
    
    if ($('#fx-onscroll-santa').length > 0) {
        destroyScrollMagic();
        if ($(window).width() > 992) {
            animationOnScroll();
        } else {
            destroyScrollMagic();
        }
    }
});

$(window).on('load', function() {
    if ($('#fx-onscroll-santa').length > 0) {
        if ($(window).width() > 992) {
            animationOnScroll();
        } else {
            destroyScrollMagic();
        }
    }
});


$(window).on('scroll', function() {    
    if($('#video').length > 0) {
        if(($(window).scrollTop() + $('header').height()) > ($('.main-wrapper').offset().top)) {
            $('#video').prop({ muted: true });
            $('.volume-off').removeClass('hidden');
            $('.volume-on').addClass('hidden');
        } else {
            $('#video').prop({ muted: false });
            $('.volume-off').addClass('hidden');
            $('.volume-on').removeClass('hidden');
        }
    }
});


$(function () {
    cookies();
    textOrphans();

    christmasBalls();
    wishes();
    previewThumbImage();

    if (document.body.contains(document.getElementById('animation-placeholder'))) {
        countdown();
        santaAnimationPlaceholder();
    }
});