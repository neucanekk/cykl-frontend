const gulp = require("gulp");
const babel = require('gulp-babel');
const browsersync = require("browser-sync").create();
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const buffer = require('vinyl-buffer');
const fileinclude = require('gulp-file-include');
const minifyCSS = require('gulp-clean-css');
const gutil = require('gulp-util');
const concat = require('gulp-concat');
const streamqueue = require('streamqueue');

const source = {
    css: {
        dev: './public/css',
        prod: '../public/css'
    },
    js: {
        dev: './public/js',
        prod: '../public/js'
    }
}

// BrowserSync
function browserSync(done) {
    browsersync.init({
        server: {
            baseDir: "./public",
            index: 'index.html'
        },
        port: 3000,
        serveStaticOptions: {
            extensions: ['html']
        },
        // online: true,
        // tunnel: true
    });
    done();
}

// BrowserSync Reload
function browserSyncReload(done) {
    browsersync.reload();
    done();
}

//styles
function styles() {
    return gulp.src('src/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 9', 'ff 17', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(minifyCSS({ compatibility: 'ie8' }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(source.css.dev))
        // .pipe(gulp.dest(source.css.prod))
}

//includes
function includes() {
    return gulp.src(['src/public/*.html', 'src/public/**/*.html'])
        .pipe(fileinclude())
        .pipe(gulp.dest('./public'));
}

//vendors
function vendors() {
    return streamqueue({
        objectMode: true
    },
        gulp.src([
            './src/js/vendors/jquery.js',
            // './src/js/vendors/matchHeight.js',
            // './src/js/vendors/owlcarousel.js',
            './src/js/vendors/vue.js',
            // './src/js/vendors/vuex.min.js',
            // './src/js/vendors/vue.min.js',
            // './src/js/vendors/vue-router.js',
            // './src/js/vendors/vue-router.min.js',
            // './src/js/vendors/selectric.js',
            // './src/js/vendors/slick.js',
            // './src/js/vendors/dragscroll.js',
            // './src/js/vendors/masonry.pkgd.js',
            './src/js/vendors/gsap.js',
            './src/js/vendors/ScrollTrigger.min.js',
            './src/js/vendors/ScrollMagic/ScrollMagic.js',
            './src/js/vendors/ScrollMagic/plugins/debug.addIndicators.js',
            './src/js/vendors/ScrollMagic/plugins/animation.gsap.js',
            './src/js/vendors/cookie.js',
            './src/js/vendors/lottie-web.js',
            './src/js/vendors/cropper.min.js',
            // './src/js/vendors/mCustomScrollbar.min.js',
            // './src/js/vendors/jquery.custom-scrollbar.min.js',
            // './src/js/vendors/axios.min.js',
            // './src/js/vendors/plyr.min.js',
            // './src/js/vendors/nanoScroller.js',
            // './src/js/vendors/scrollIt.js',
            // './src/js/vendors/countUp.js',
            // './src/js/vendors/footable.js',
            // './src/js/vendors/retina.js',
            // './src/js/vendors/es6-promise-shim.min.js',
            // './src/js/vendors/metisMenu.js',
            // './src/js/vendors/select2.full.js',
            // './src/js/vendors/select2.full.pl.js',
            // './src/js/vendors/footable.js',
            // './src/js/vendors/d3.v3.js',
            // './src/js/vendors/c3.js',
            // './src/js/vendors/velocity.js',
            // './src/js/vendors/velocity-ui.js',
            // './src/js/vendors/perfect-scrollbar.js',
            // './src/js/vendors/modernizr-custom.js',
            // './src/js/vendors/moment.js',
            // './src/js/vendors/autosize.js',
            // './src/js/vendors/hammer.js',
            // './src/js/vendors/parallax.js',
            // './src/js/vendors/parsley.js',
            // './src/js/vendors/parsley-pl.js',
            // './src/js/vendors/retina.js',
            // './src/js/vendors/scrolldetector.js',
            // './src/js/vendors/scrolling.js',
            // './src/js/vendors/scrollIt.js',
            // './src/js/vendors/sticky-kit.js',
            // './src/js/vendors/waypoints.js',
        ]))
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        .pipe(concat('vendors.js'))
        .pipe(uglify())
        .on('error', gutil.log)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(source.js.dev))
        // .pipe(gulp.dest(source.js.prod))
}

//scripts
function scripts() {
    return streamqueue(
        {
            objectMode: true
        },

        gulp.src([
            './src/js/polyfills/*.js',
            './src/js/components/*.js',
            './src/js/vue/*.js',
            './src/js/custom.js'
        ])
    )
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        .pipe(concat('main.js'))
        .pipe(buffer())
        .pipe(babel())
        .pipe(uglify())
        .on('error', gutil.log)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(source.js.dev))
        // .pipe(gulp.dest(source.js.prod))
}

//scripts
function dragndrop() {
    return streamqueue(
        {
            objectMode: true
        },

        gulp.src([

            './src/js/dragndrop.js',

        ])
    )
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        .pipe(concat('dragndrop.js'))
        .pipe(buffer())
        .pipe(babel())
        .pipe(uglify())
        .on('error', gutil.log)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(source.js.dev))
        // .pipe(gulp.dest(source.js.prod))
}

function mobileScripts() {
    return streamqueue(
        {
            objectMode: true
        },

        gulp.src([
            './src/js/polyfills/*.js',
            './src/js/components/*.js',
            './src/js/vue/*.js',
            './src/js/mobile/app.js'
        ])
    )
    .pipe(sourcemaps.init({
        loadMaps: true
    }))
    .pipe(concat('mobile-app.js'))
    .pipe(buffer())
    .pipe(babel())
    .pipe(uglify())
    .on('error', gutil.log)
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(source.js.dev))
    // .pipe(gulp.dest(source.js.prod))
}

// Watch files
function watchFiles() {
    gulp.watch(
        [
            "./src/**/*"
        ],
        gulp.series(browserSyncReload)
    );
    gulp.watch([
        './src/static/**/*.html',
        './src/static/**/**/*.html',
        'src/public/*.html'
    ], includes);
    gulp.watch("src/scss/**/*.scss", styles);
    gulp.watch([
        './src/js/polyfills/*.js',
        './src/js/components/*.js',
        './src/js/vue/*.js',
        './src/js/custom.js',
    ], scripts);
    gulp.watch([
        './src/js/dragndrop/dragndrop.js',
    ], dragndrop);
    gulp.watch([
        // Mobile Scripting
        './src/js/polyfills/*.js',
        './src/js/components/*.js',
        './src/js/vue/*.js',
        './src/js/mobile/app.js'
    ], mobileScripts);
}

//define complex tasks
const watch = gulp.parallel(watchFiles, browserSync);
const build = gulp.parallel(includes, styles, vendors, scripts, dragndrop, mobileScripts);

// export tasks
exports.watch = watch;
exports.build = build;
exports.styles = styles;
exports.scripts = scripts;
exports.dragndrop = dragndrop;
exports.vendors = vendors;
exports.includes = includes;

//default task
gulp.task('default', gulp.series(gulp.parallel(watchFiles, browserSync)))
