'use strict';

// Functions
var cookies = function () {
    if ($.cookie('giodoCookies') !== '1') {
        $('.cookies-alert').css('display', 'block');
    }
    $('.cookies-alert button').on('click', function () {
        $('.cookies-alert').slideToggle('slow', function () {
            $.cookie('giodoCookies', '1', {
                expires: 365
            });
        });
    });
};

var mobileSantaSequence = function() {
    var santaId = $('#santaSequence');

    santaId.attr("src", "/img/sequence/santa-cropped/santa_animation_static-mobile.png");
}

var appHeight = function() {
    var app = $('#app');
    var isHeight = $(window).height();

    app.height(isHeight - 75);
}

var onNavScrollHandler = function() {
    var videoContainer = $('header .header-video');
    var menu = $('.main-nav');
    var windowTop = $(window).scrollTop();

    if(videoContainer.length > 0) {
        if(windowTop > (videoContainer.offset().top + videoContainer.height())) {
            menu.addClass('is-scrolling');
        } else {
            menu.removeClass('is-scrolling');
        }
    }
}

var santaAnimationPlaceholder = function () {
    const player = document.querySelector("lottie-player");
    player.load('../json/santa-placeholder.json');
}

var countdown = function () {
    var countDownDate = new Date('Dec 1, 2021 00:00:00').getTime();

    var x = setInterval(function () {
        var now = new Date().getTime();

        var distance = countDownDate - now;

        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        var leadingZero = function (count, size) {
            count = count.toString();
            while (count.length < size) count = "0" + count;
            return count;
        }
        var countDays = $('#days');
        var countHours = $('#hours');
        var countMinutes = $('#minutes');
        var countSeconds = $('#seconds');

        countDays.text(leadingZero(days, 2));
        countHours.text(leadingZero(hours, 2));
        countMinutes.text(leadingZero(minutes, 2));
        countSeconds.text(leadingZero(seconds, 2));

        if (distance < 0) {
            clearInterval(x);
        }
    }, 1000);
}

// Events
$('.hamburger').on('click', function () {
    $(this).toggleClass('is-active');

    $('.navigation-drawer').removeClass('active-drawer');
    $('.hamburger-inner').css({
        backgroundColor: ''
    });

    if ($(this).hasClass('is-active')) {
        $('.navigation-drawer').addClass('active-drawer');
        $('.hamburger-inner').css({
            backgroundColor: '#359530'
        });
    }
});

$('#mobile-nav li').on('click', function (e) {
    e.preventDefault();

    $(this).each(function () {
        var anchor = $(this).find('a')

        $('.navlink a').removeClass('active');
        anchor.addClass('active');
    });
});

$('.menu-nav:not(.menu-nav-ar) ul li a').on('click', function(e){
    e.preventDefault();
    var that = $(this).attr('href');

    $('html, body').animate({
        scrollTop: $(that).offset().top
    }, 'slow');
});


$('#play-video').on('click', function() {
    var videoContainer = $('#video-container');
    var video = $('#video');
    var volumeNav = $('#volume-settings');
    var volumeIconOn = volumeNav.find($('.volume-on'));
    var volumeIconOff = volumeNav.find($('.volume-off'));

    $('.control-play').removeClass('visible');
    videoContainer.removeClass('background-video');
    video.removeAttr('muted');
    video.prop({muted: false, autoplay: false, loop: false, controls: true});
        
    
    if(video.prop('muted')) {        
        volumeIconOn.addClass('hidden');        
        volumeIconOff.removeClass('hidden');        
    } else {
        volumeIconOff.addClass('hidden');        
        volumeIconOn.removeClass('hidden');    
    }
});

$('#volume-settings').click(function () {
    if ($('#video').prop('muted')) {
        $('#video').prop('muted', false);
        $('.volume-off').addClass('hidden');        
        $('.volume-on').removeClass('hidden');        
    } else {
        $('#video').prop('muted', true);        
        $('.volume-on').addClass('hidden');        
        $('.volume-off').removeClass('hidden');        
    }
});

// $('#video-fullscreen .fullscreen').click(function() {
//     if($('.fullscreen').hasClass('hidden')) {
//         $('.fullscreen').removeClass('hidden');
//         $(this).addClass('hidden');
//     }
// }); 

$(window).on('load', function() {
    appHeight();
});
$(window).on('resize', function() {
    appHeight();
    onNavScrollHandler();
});

$(window).on('scroll', function() {
    onNavScrollHandler();
    if($('#video').length > 0) {
        if($(window).scrollTop() > ($('.main-wrapper').offset().top / 1.5)) {
            $('#video').prop({ muted: true });
            $('.volume-off').removeClass('hidden');        
            $('.volume-on').addClass('hidden');
            // $('#video').get(0).pause();
        } else {
            $('#video').prop({ muted: false });
            $('.volume-off').addClass('hidden');        
            $('.volume-on').removeClass('hidden');   
            // $('#video').get(0).play();
        }
    }
});

$(function () {
    cookies();
    mobileSantaSequence();

    if (document.body.contains(document.getElementById('animation-placeholder'))) {
        countdown();
        santaAnimationPlaceholder();
    }
});