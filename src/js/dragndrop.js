let dragged;
let oldItem;

document.addEventListener("dragstart", function (event) {
    dragged = ""; // reset item
    oldItem = "";
    if (event.target.classList.contains('gallery-item') && !event.target.classList.contains('gallery-item-dropped')) { //only item with certain class and prevent to drag again
        dragged = event.target.querySelector('img');
        oldItem = event.target;
    }
    console.log(event.target.classList, 'cropped-item-image')
    if (event.target.classList.contains('user-gallery-item') && !event.target.classList.contains('gallery-item-dropped')) {
        dragged = event.target;
        oldItem = document.querySelector('#' + event.target.dataset.id + ' .cropped-item-wrapper');
    }
}, false);

document.addEventListener("dragover", function (event) {
    // prevent default to allow drop
    event.preventDefault();
}, false);

document.addEventListener("drop", function (event) {
    // prevent default action (open as link for some elements)
    event.preventDefault();

    if (event.target.classList.contains('dropzone')) {
        let selectedItem = document.querySelectorAll('.gallery-list .gallery-item-dropped')
        if ((selectedItem.length <= 12) && (dragged)) {
            let elementXmassBall = document.querySelector('#' + event.target.id);
            elementXmassBall.innerHTML = "";
            let imgElement = document.createElement('img')
            imgElement.setAttribute('class', 'gallery-image-dropped')
            imgElement.setAttribute('src', dragged.getAttribute('src').toString())
            if (dragged.classList.contains('user-gallery-item')) {
                dragged.classList.remove('user-gallery-item');
                let elementImageDiv = document.querySelector('#' + dragged.dataset.id).querySelector('.cropped-item-wrapper');
                elementImageDiv.classList.add('gallery-item-dropped', 'selected');
                elementImageDiv.innerHTML = "";
                elementImageDiv.appendChild(imgElement)
            } else {
                let elementImageDiv = document.querySelector('#' + oldItem.id);
                elementImageDiv.classList.add('gallery-item-dropped', 'selected');
                elementImageDiv.innerHTML = "";
                elementImageDiv.appendChild(imgElement);
            }
            elementXmassBall.appendChild(dragged);
        }
    }
}, false);

document.addEventListener('change', function (event) {
    event.preventDefault();
    let uploadSelected = event.target;
    let closeModalIcon = document.getElementById('close-crop-modal');

    uploadSelected.removeAttribute('data-upload');
    closeModalIcon.removeAttribute('data-close');

    uploadSelected.setAttribute('data-upload', uploadSelected.id);
    closeModalIcon.setAttribute('data-close', uploadSelected.id);

    if (event.target.classList.contains('user-image-input')) {
        let cropBox = document.querySelector('.crop-image-modal');
        let result = document.querySelector('.crop-result');

        if (event.target.files.length) {
            const reader = new FileReader();

            reader.onload = readSuccess;

            function readSuccess(event) {
                cropBox.classList.add('active');
                cropBox.setAttribute('data-id', uploadSelected.id);

                let img = document.createElement('img');
                img.id = 'upload-' + cropBox.dataset.id;
                img.src = event.target.result;

                result.innerHTML = '';
                result.appendChild(img);
            };

            reader.readAsDataURL(event.target.files[0]);
        }
    }
});


// document.addEventListener('click', function (event) {
//             event.preventDefault();
//             const button = document.getElementById('card_download');
//             if (button.length !== {
//                     const card = document.getElementById('card_generator');
//                     htmlToImage.toPng(card)
//                     .then(function (dataUrl) {
//                         download(dataUrl, 'kartka.png');
//                     });
//                 }
//             });